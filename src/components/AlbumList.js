// we don't put React in the destructuring braces because
// it's needed when JSX is transformed into HTML
import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import AlbumDetail from './AlbumDetail';

// we use class instead of const when component is going
// to pack more functionality than just presenting data
// use then accessing state or having lifecycle methods (e.g. componentWillMount)
class AlbumList extends Component {
  // only class based components have state
  state = { albums: [] };

  // componentWillMount executes automatically when component is loaded
  componentWillMount() {
    fetch('https://rallycoding.herokuapp.com/api/music_albums')
      .then((response) => response.json())
      .then((responseData) => {
        // always use setState function instead of directly
        // referencing the variable state !!!
        this.setState({ albums: responseData });
      });
  }

  renderAlbums() {
    return this.state.albums.map(album =>
      // key should be unique thefore best practice is to use id for it
      <AlbumDetail key={album.title} album={album} />
    );
  }

  render() {
    console.log(this.state);
    return (
      <ScrollView>
        {this.renderAlbums()}
      </ScrollView>
    );
  }
}

export default AlbumList;
